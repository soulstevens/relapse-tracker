#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

// Globals
char * habit;

// Function to create .relapse and return path
char * createdotrelapse(){
	char * path = (char *) calloc(128, sizeof(char));
	char * homedir = getenv("HOME");
	if (!homedir){
		printf("Error finding home directory");
		exit(0);
	}

	strcpy(path, homedir);
	strcat(path, "/.relapse");

	int check = mkdir(path, 0775);
	if (!mkdir){
		printf("Created .relapse directory\n");
	}
	return path;
}

// Function to get path
void getpath (char * path){
	char * dotrelapsepath = createdotrelapse();
	strcat(path, dotrelapsepath); free(dotrelapsepath);
	strcat(path, "/");
	strcat(path, habit);
}

// Function to get array of relapse timestamps
unsigned long * gettimestamps(){
	char path [128];
	path[0] = '\0';	// For some reason the 1st char is '@'
	getpath(path);

	FILE * fp;
	fp = fopen(path, "r+");
	if (fp == NULL){
		printf("Error opening file\n");
		return NULL;
	}

	// Work out how many timestamps are in the file
	int count = 0;
	char c;
	while (1){
		c = fgetc(fp);
		if (feof(fp))	break;
		if (c == '\n') 	count++;
	}
	fseek(fp, 0, SEEK_SET);
	unsigned long * timestamps = (unsigned long *) calloc(count, sizeof(unsigned long));
	char str_ts [11];
	for (int i=0; i<count; i++){
		fgets(str_ts, 11, fp);
		timestamps[i] = strtoul(str_ts, NULL, 0);
		fgetc(fp); // Skip EOL... I think
	}
	return timestamps;
}

// Method tp reset streak
void reset (){
	printf("Resetting streak\n");
	unsigned long ts = (unsigned long) time (NULL);
	char path [128];
	getpath(path);

	FILE * fp;
	fp = fopen(path, "a+");
	if (fp == NULL){
		printf("Error opening file %s\n", path);
		exit(0);
	}
	fprintf(fp, "%lu\n", ts);
	fclose(fp);

}

// Function to get hours and days from seconds
void gethoursanddays (int * hours, int * days, int seconds){
	*hours = seconds / 60 / 60;
	*days = *hours / 24;
	*hours -= *days * 24;
}


//Method to show current streak
void longeststreak (){
	unsigned long * timestamps = gettimestamps();
	int length = sizeof timestamps / sizeof timestamps[0];
	unsigned long longeststreak, difference;
	for (int i=0; i<length; i++){
		if (i == length - 1){
			difference = (unsigned long) time (NULL) - timestamps[i];
		}
		else {
			difference = timestamps[i+1] - timestamps[i];
		}

		if (i == 0){
			longeststreak = difference;
		}
		else if (difference > longeststreak){
			longeststreak = difference;
		}
	}
	free(timestamps);
	int hours, days;
	gethoursanddays(&hours, &days, longeststreak);
	printf("Your longest streak is %d day%c and %d hour%c\n",
		days,
		days != 1 ? 's' : '\0',
		hours,
		hours != 1 ? 's' : '\0' );
}

void currentstreak(){
	unsigned long * timestamps = gettimestamps();
	int length = sizeof timestamps / sizeof timestamps[0];
	int hours, days;
	gethoursanddays(&hours, &days, (unsigned long)time(NULL) - timestamps[length-1]);
	free(timestamps);
	printf("Your current streak is %d day%c and %d hour%c\n",
		days,
		days != 1 ? 's' : '\0',
		hours,
		hours != 1 ? 's' : '\0' );
}

// Function to get position of value in command line argument
int getcla (int argc, char *argv[], char * _switch){
	int gethabit = _switch == NULL;
	int value = 0;
	int i = 1;
	while (i < argc){
		if (gethabit){
			if (argv[i][0] != '-'){
				return i;
			}
		}
		else{
			if (strcmp(argv[i], _switch) == 0){
				return i+1;
			}
		}
		i+= 1 + gethabit;
	}
	return 0;
}

int main (int argc, char *argv[]){

	int habit_idx = getcla(argc, argv, NULL);
	if (habit_idx == 0){
		printf("Habit has not been specified");
		exit(-1);
	} else{
		habit = argv[habit_idx];
	}

	int operation_idx = getcla(argc, argv, "-c");
	char operation = '\0';
	if (operation_idx != 0){
		operation = argv[operation_idx][0];
	}

	switch (operation){
		case 's':
			longeststreak();
			break;
		case 'r':
			reset();
			break;
	}
	currentstreak();
	return 0;
}
